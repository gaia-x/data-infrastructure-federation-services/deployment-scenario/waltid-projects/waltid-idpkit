package system

import future.keywords.in 

default main = false

format_vc_url(x) := res {
    tmp := replace(x,":","/")
    res := replace(tmp, "did/web/","https://")
    print(res)
}

get_vc(url) := res {
	http_response := http.send({
		"url": url,
		"method": "get"
	})
    res := http_response.body
    print(res)
}


main {
    input.credentialData.type[count(input.credentialData.type) -1] != "gx:ParticipantCredential"
} else {
    input.credentialData.type[count(input.credentialData.type)-1] == "gx:ParticipantCredential"
    x := input.credentialData["credentialSubject"]["id"]
    url := format_vc_url(x)
    vc := get_vc(url)
    vc["credentialSubject"]["gx:headquarterAddress"]["gx:countrySubdivisionCode"] == "IT-25"
    print(count(vc["@context"]))
}
